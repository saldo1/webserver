// Imports

const express = require("express");
const app = express();
const ip = "192.168.178.66";
const PORT = 8080;

//const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const cors = require("cors");

const assert = require("assert");

const parse = require("parse-color");
const moment = require("moment");

const multer = require("multer");
const path = require("path");

// Database
/*const uri = "mongodb://localhost:27017/InstaGalleryDB";
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true }, () =>
  console.log("Connected to DB")
);*/

const database = require("./public/js/database");
database.connectToDatabase();

// Static Files
app.use(express.static("public"));
app.use("/css", express.static(__dirname + "public/css"));
app.use("/js", express.static(__dirname + "public/js"));
app.use("/img", express.static(__dirname + "public/img"));
app.use("/database", express.static(__dirname + "public/database"));

// Set Views
app.set("views", "./views");
app.set("view engine", "ejs");

// Import Routes
const channelsRoute = require("./routes/channels");
const picturesRoute = require("./routes/pictures");
app.use("/channels", channelsRoute);
app.use("/pictures", picturesRoute);

// Routes
{
  app.get("", (req, res) => {
    //res.render("pages/channel_ejs.ejs", json);
  });
}
// Listen on port 8080
app.listen(PORT, ip || "localhost", () =>
  console.info(`Listening on ${ip}:${PORT}`)
);
