var channel = JSON.parse(channelString);

var canvas = document.createElement("canvas");

var color = channel.websitedata;

var getColor = false;
var whichColor = 0;

var img = document.getElementsByClassName("img")[0];
img.crossOrigin = "anonymous";
img.src = channel.profilepicture;
// https://cors-anywhere.herokuapp.com/
img.addEventListener("load", draw, false);

function draw() {
  if (img.naturalWidth > vw(30)) {
    canvas.width = vw(30);
    canvas.height = (img.naturalHeight * canvas.width) / img.naturalWidth;
  } else {
    canvas.width = img.naturalWidth;
    canvas.height = img.naturalHeight;
  }

  canvas.getContext("2d").drawImage(img, 0, 0, canvas.width, canvas.height);

  document.body.getElementsByClassName("leftside")[0].insertBefore(canvas, img);

  canvas.addEventListener("mousedown", (event) => {
    getColor = true;
    printMousePos(event);
  });

  canvas.addEventListener("mouseup", () => {
    getColor = false;
  });

  canvas.addEventListener("mousemove", printMousePos);
}

function printMousePos(event) {
  if (getColor && whichColor > 0) {
    var pixelData = canvas
      .getContext("2d")
      .getImageData(
        event.clientX - canvas.offsetLeft,
        event.clientY - canvas.offsetTop,
        1,
        1
      ).data;

    if (whichColor == 1) {
      color.clr_background =
        "" + RGBToHex(pixelData[0], pixelData[1], pixelData[2]);
    }
    if (whichColor == 2) {
      color.clr_accent =
        "" + RGBToHex(pixelData[0], pixelData[1], pixelData[2]);
    }
    color.clr_primary =
      "" + backgroundBlackDisplacement(color.clr_background, 20);
    color.clr_secondary =
      "" + backgroundBlackDisplacement(color.clr_background, 40);
    update();
  }
}

const nameInput = document.getElementsByClassName("nameInput")[0];
nameInput.value = channel.name;
nameInput.addEventListener("change", (event) => {
  update();
});

const occupationInput = document.getElementsByClassName("occupationInput")[0];
occupationInput.value = channel.occupation;
occupationInput.addEventListener("change", (event) => {
  update();
});

const tagsInput = document.getElementsByClassName("tagsInput")[0];
tagsInput.value = channel.tags;
tagsInput.addEventListener("change", (event) => {
  update();
});

const pictureInput = document.getElementsByClassName("pictureInput")[0];
pictureInput.addEventListener("change", (event) => {
  const [file] = pictureInput.files;
  if (file) img.src = URL.createObjectURL(file);
  draw();
});

const backgroundInput = document.getElementsByClassName("background")[0];
backgroundInput.addEventListener("click", () => {
  whichColor = 1;
});

const accentInput = document.getElementsByClassName("accent")[0];
accentInput.addEventListener("click", () => {
  whichColor = 2;
});

var rad = document.minorselector.minorcolor;
for (var i = 0; i < rad.length; i++) {
  rad[i].addEventListener("change", () => {
    update();
  });
}

var radios = document.getElementsByName("minorcolor");

function getMinorColor() {
  for (var i = 0, length = radios.length; i < length; i++) {
    if (radios[i].checked) {
      // do whatever you want with the checked radio
      if (i == 0) color.clr_minor = "#ffffff";
      else color.clr_minor = "#000000";

      // only one radio can be logically checked, don't check the rest
      break;
    }
  }
}

function update() {
  getMinorColor();
  var data = {
    name: nameInput.value,
    urls: channel.urls,
    tags: tagsInput.value.split(","),
    occupation: occupationInput.value,
    profilepicture: "/img/placeholder.png",
    websitedata: {
      clr_background: color.clr_background,
      clr_primary: color.clr_primary,
      clr_secondary: color.clr_secondary,
      clr_accent: color.clr_accent,
      clr_minor: color.clr_minor,
      img_align: "center",
    },
  };

  fetch("/channels/api/" + channel._id, {
    method: "PATCH",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json; charset=UTF-8",
    },
  })
    .then((response) => response.json())
    .then(() => {
      var iframe = document.getElementsByClassName("preview")[0];
      iframe.src = iframe.src;
    });
}

function RGBToHex(r, g, b) {
  r = r.toString(16);
  g = g.toString(16);
  b = b.toString(16);

  if (r.length == 1) r = "0" + r;
  if (g.length == 1) g = "0" + g;
  if (b.length == 1) b = "0" + b;

  return "#" + r + g + b;
}

function backgroundBlackDisplacement(background, displacement) {
  // Convert HEX to HSL
  var color = HEXtoHSL(background);
  // Displace Black
  if (color.l > 50) color.l = displacement;
  else color.l = 100 - displacement;
  // Convert HSL to HEX
  return h_s_lToHex(color.h, color.s, color.l);
}

function vw(v) {
  var w = Math.max(
    document.documentElement.clientWidth,
    window.innerWidth || 0
  );
  return (v * w) / 100;
}

function HEXtoHSL(hex) {
  hex = hex.replace(/#/g, "");
  if (hex.length === 3) {
    hex = hex
      .split("")
      .map(function (hex) {
        return hex + hex;
      })
      .join("");
  }
  var result = /^([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})[\da-z]{0,0}$/i.exec(hex);
  if (!result) {
    return null;
  }
  var r = parseInt(result[1], 16);
  var g = parseInt(result[2], 16);
  var b = parseInt(result[3], 16);
  (r /= 255), (g /= 255), (b /= 255);
  var max = Math.max(r, g, b),
    min = Math.min(r, g, b);
  var h,
    s,
    l = (max + min) / 2;
  if (max == min) {
    h = s = 0;
  } else {
    var d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
    switch (max) {
      case r:
        h = (g - b) / d + (g < b ? 6 : 0);
        break;
      case g:
        h = (b - r) / d + 2;
        break;
      case b:
        h = (r - g) / d + 4;
        break;
    }
    h /= 6;
  }
  s = s * 100;
  s = Math.round(s);
  l = l * 100;
  l = Math.round(l);
  h = Math.round(360 * h);

  return {
    h: h,
    s: s,
    l: l,
  };
}

function h_s_lToHex(h, s, l) {
  h /= 360;
  s /= 100;
  l /= 100;
  var r, g, b;
  if (s === 0) {
    r = g = b = l;
  } else {
    const hue2rgb = function (p, q, t) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1 / 6) return p + (q - p) * 6 * t;
      if (t < 1 / 2) return q;
      if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
      return p;
    };
    const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    const p = 2 * l - q;
    r = hue2rgb(p, q, h + 1 / 3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1 / 3);
  }
  const toHex = function (x) {
    const hex = Math.round(x * 255).toString(16);
    return hex.length === 1 ? "0" + hex : hex;
  };
  return "#" + toHex(r) + toHex(g) + toHex(b);
}
