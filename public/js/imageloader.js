const fetch = require("node-fetch");

var imagesrc = async function (link) {
  return await getInstagramURL(link);
};

async function getInstagramURL(link) {
  var src = await getRedirectedUrl(link + "media?size=l");
  return "https://cors-anywhere.herokuapp.com/" + src;
  //return "https://cors-anywhere.herokuapp.com/" + link;
  //return link;
}

async function getRedirectedUrl(url) {
  var response = await fetch(url, {
    method: "GET",
  });
  return response.url;
}

module.exports = imagesrc;
