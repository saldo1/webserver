const mongoose = require("mongoose");

const Channel = require("../../data/models/Channel");
const Entry = require("../../data/models/Entry");

var mysql = require("mysql");
var db;

var test = {
  connectToDatabase() {
    /*const uri = "mongodb://localhost:27017/InstaGalleryDB";
    mongoose.connect(
      uri,
      { useNewUrlParser: true, useUnifiedTopology: true },
      () => console.log("Connected to DB")
    );*/

    db = mysql.createConnection({
      host: "localhost",
      user: "nodejs_server",
      password: "KMr5H8LiM2X8@hC5",
      database: "insta_gallery_db",
    });
    console.log("Connection to MySQL database created");
  },

  async getAllChannels() {
    return new Promise(function (resolve, reject) {
      db.query(
        "SELECT name, channel_url, `picture`.`image_src`\
        FROM `channel` \
            LEFT JOIN `picture` ON `channel`.`profile_picture_id` = `picture`.`picture_id`;",
        function (err, result, fields) {
          if (err) reject(err);
          resolve(result);
        }
      );
    });
  },

  async getChannelByUrl(url) {
    return new Promise(function (resolve, reject) {
      db.query(
        'SELECT channel.*, `picture`.`image_src`\
          FROM `channel` \
              LEFT JOIN `picture` ON `channel`.`profile_picture_id` = `picture`.`picture_id`\
          WHERE `channel`.`channel_url` = "' +
          url +
          '" LIMIT 1;',
        function (err, result, fields) {
          if (err) reject(err);
          resolve(result);
        }
      );
    });
  },

  async getTagsFromChannel(channel_id) {
    return new Promise(function (resolve, reject) {
      db.query(
        "SELECT `tag`.`tag_name`\
        FROM `tag` \
            LEFT JOIN `channel_x_tag` ON `channel_x_tag`.`tag_id` = `tag`.`tag_id`\
        WHERE `channel_x_tag`.`channel_id` = " +
          channel_id +
          ";",
        function (err, result, fields) {
          if (err) reject(err);
          resolve(result);
        }
      );
    });
  },

  async getAllPictures() {
    return new Promise(function (resolve, reject) {
      db.query("SELECT * from picture", function (err, result, fields) {
        if (err) reject(err);
        resolve(result);
      });
    });
  },
};
module.exports = test;
