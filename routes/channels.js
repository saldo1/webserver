const express = require("express");
const router = express.Router();
const database = require("../public/js/database");

// RENDER all channels
router.get("/", async (req, res) => {
  try {
    database.getAllChannels().then(function (results) {
      res.render("../views/pages/channels_ejs.ejs", {
        channels: results,
      });
    });
  } catch (err) {
    res.json({ message: err });
  }
});

// RENDER a specific channel
router.get("/:url", async (req, res) => {
  try {
    database.getChannelByUrl(req.params.url).then(function (resultChannel) {
      resultChannel[0].profile_colors = JSON.parse(
        resultChannel[0].profile_colors
      );
      database
        .getTagsFromChannel(resultChannel[0].channel_id)
        .then(function (resultTags) {
          res.render("../views/pages/channel_ejs.ejs", {
            channel: Object.assign({}, resultChannel[0], { tags: resultTags }),
          });
        });
    });
  } catch (err) {
    res.json(err);
  }
});

module.exports = router;
