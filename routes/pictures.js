const express = require("express");
const database = require("../public/js/database");
const router = express.Router();
const imageloader = require("../public/js/imageloader");

// Render all pictures
router.get("/", async (req, res) => {
  try {
    database.getAllPictures().then(function (results) {
      res.render("../views/pages/pictures_ejs.ejs", {
        pictures: results,
      });
    });
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
