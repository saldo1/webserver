const mongoose = require("mongoose");

const ChannelSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  occupation: {
    type: String,
  },
  profilepicture: {
    type: String,
  },
  websitedata: {
    type: Object,
  },
  tags: {
    type: Array,
  },
  urls: {
    type: Array,
    required: true,
  },
});

module.exports = mongoose.model("Channel", ChannelSchema);
