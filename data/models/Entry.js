const { ObjectId } = require('bson');
const mongoose = require('mongoose');

const EntrySchema = mongoose.Schema({
    urls: {
        type: Array,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    tags:{
        type: [{tag: String}]
    },
    starId:{
        type: ObjectId,
        required: true
    }
});

module.exports = mongoose.model('Entries', EntrySchema);